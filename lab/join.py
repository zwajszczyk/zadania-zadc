# -*- coding: utf-8 -*-

import json
from MapReduce import MapReduce


def map(item):
    key = item[1]
    value1 = item[0]
    value2 = item[2:]
    value = [value1, value2]
    #print (key, value)
    yield (key, value)
    pass


def reduce(key, values):
    iresult = []
    orders = []
    items = []
    results = []

    for val in values:
        if val[0]=="order":
            neworder = []
            neworder.append(val[0])
            for pos in val[1]:
                neworder.append(pos)
            orders.append(neworder)
        elif val[0]=="line_item":
            newitem = []
            newitem.append(val[0])
            for pos in val[1]:
                newitem.append(pos)
            items.append(newitem)
    #print orders
    for order in orders:
        for item in items:
            iresult = []
            iresult.append(key)
            for pos in order:
                iresult.append(pos)
            for pos in item:
                iresult.append(pos)
            results.append(iresult)
    #print results
    return results
    # #result.append([])
    # iresult.append(key)
    # for value in values:
    #     iresult.append(value)
    # #return result
    pass


if __name__ == '__main__':
    #input_data = ...
    myfile = open("records.json")
    #input_data = json.loads(myfile)
    input_data = []
    for line in myfile:
        input_data.append(json.loads(line))
    #print input_data

    mapper = MapReduce(map, reduce)
    tempresults = mapper(input_data)
    results = []
    for tresult in tempresults:
        for item in tresult:
            results.append(item)
    for row in results:
        print json.dumps(row)
